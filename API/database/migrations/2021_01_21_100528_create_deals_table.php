<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration
{
    /*Todo
    category
    deal_category

    user_tracking



    createDeal
    EditDeal
    DeleteDeal
    UpdateViews
    getDeal
    getdeals

     *
     *
     *
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->text('description');
            $table->string('image',255);
            $table->string('url',255);
            $table->string('offer_type',50)->nullable();
            $table->string('coupon',25)->nullable();
            $table->integer('price');
            $table->integer('special_price');
            $table->integer('rating');
            $table->integer('store_id');
            $table->integer('viewed')->default(0);
            $table->integer('created_by');
            $table->integer('reviewed_by')->nullable();
            $table->integer('status');
            $table->date('expires_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
