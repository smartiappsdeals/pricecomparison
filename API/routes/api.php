<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| remove
*/

Route::post('login', 'App\Http\Controllers\AuthController@login');
Route::post('register', 'App\Http\Controllers\AuthController@signUp');
Route::get('/', 'App\Http\Controllers\AuthController@loginpage')->name('login');

//API for deals
Route::post('deals', 'App\Http\Controllers\DealsController@create_deals');
Route::get('deals','App\Http\Controllers\DealsController@getDeals');
Route::get('deals/{deal}','App\Http\Controllers\DealsController@getDeal');
Route::put('deals/{deal}','App\Http\Controllers\DealsController@updatedeal');
Route::delete('deals/{deal}','App\Http\Controllers\DealsController@deletedeal');

//API for country
Route::post('country', 'App\Http\Controllers\CountryController@create_country');
Route::get('country', 'App\Http\Controllers\CountryController@getCountries');
Route::get('country/{country}', 'App\Http\Controllers\CountryController@getcountry');
Route::put('country/{country}','App\Http\Controllers\CountryController@updatecountry');
Route::delete('country/{country}','App\Http\Controllers\CountryController@deletecountry');

//API for store
Route::post('stores', 'App\Http\Controllers\StoresController@create_stores');
Route::get('stores', 'App\Http\Controllers\StoresController@getstores');
Route::get('stores/{store}', 'App\Http\Controllers\StoresController@getstore');
Route::put('stores/{store}','App\Http\Controllers\StoresController@updateStore');
Route::delete('stores/{stores}','App\Http\Controllers\StoresController@deleteStore');

Route::resource('products','App\Http\Controllers\ProductsController');

Route::resource('types', 'App\Http\Controllers\TypesController');



Route::group([
    'middleware' => 'auth:api'
], function () {
    Route::get('logout', 'App\Http\Controllers\AuthController@logout');
    Route::get('user', 'App\Http\Controllers\AuthController@user');
});