<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        return $product;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                $request->validate([
            'name' => 'required|string',
            'type' => 'required|string',
            'amazon' => 'required|numeric',
            'flipkart' => 'required|numeric',
            'reliance' => 'required|numeric',
            'status' => 'required|numeric',

        ]);
        $product=new Product([
            'name'=>$request->name,
            'type'=>$request->type,
            'amazon'=>$request->amazon,
            'flipkart'=>$request->flipkart,
            'reliance'=>$request->reliance,
            'status'=>$request->status
        ]);
        $product->save();
        return response()->json([
            'message'=>'Product added successfully'
        ],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::where('type',$id)->get();
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'url' => 'required|string',
            'image' => 'required|string',
            'price' => 'required|numeric',
            'store_id' => 'required|string',
            'status' => 'required|boolean',

        ]);
        
        $product = Product::find($id);
        $product->name =$request->name;
        $product->description =$request->description;
        $product->url  =$request->url;
        $product->image  =$request->image;
        $product->price  =$request->price;
        $product->store_id  =$request->store_id;
        $product->status  =$request->status;


        $product->save();
        return response()->json([
            'message'=>'Products updated Successfully'
        ],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        $product->delete();
        return response()->json([
            'message'=>'Customer deleted'
        ]);
    }
}
